from tkinter import *
from math import *

def number0():
    if len(tehe) > 39:
        return
    tehe.append("0")
    if len(tehe) < 5:
        ekraan.config(font = labelfont_56)
    if len(tehe) > 10:
        ekraan.config(font = labelfont_42)
    if len(tehe) > 13:
        ekraan.config(font = labelfont_28)
    sõne.set("".join(tehe))
    
def number1():
    if len(tehe) > 39:
        return
    tehe.append("1")
    if len(tehe) < 5:
        ekraan.config(font = labelfont_56)
    if len(tehe) > 10:
        ekraan.config(font = labelfont_42)
    if len(tehe) > 13:
        ekraan.config(font = labelfont_28)
    sõne.set("".join(tehe))
    

def number2():
    if len(tehe) > 39:
        return
    tehe.append("2")
    if len(tehe) < 5:
        ekraan.config(font = labelfont_56)
    if len(tehe) > 10:
        ekraan.config(font = labelfont_42)
    if len(tehe) > 13:
        ekraan.config(font = labelfont_28)
    sõne.set("".join(tehe))

def number3():
    if len(tehe) > 39:
        return
    tehe.append("3")
    if len(tehe) < 5:
        ekraan.config(font = labelfont_56)
    if len(tehe) > 10:
        ekraan.config(font = labelfont_42)
    if len(tehe) > 13:
        ekraan.config(font = labelfont_28)
    sõne.set("".join(tehe))

def number4():
    if len(tehe) > 39:
        return
    tehe.append("4")
    if len(tehe) < 5:
        ekraan.config(font = labelfont_56)
    if len(tehe) > 10:
        ekraan.config(font = labelfont_42)
    if len(tehe) > 13:
        ekraan.config(font = labelfont_28)
    sõne.set("".join(tehe))

def number5():
    if len(tehe) > 39:
        return
    tehe.append("5")
    if len(tehe) < 5:
        ekraan.config(font = labelfont_56)
    if len(tehe) > 10:
        ekraan.config(font = labelfont_42)
    if len(tehe) > 13:
        ekraan.config(font = labelfont_28)
    sõne.set("".join(tehe))

def number6():
    if len(tehe) > 39:
        return
    tehe.append("6")
    if len(tehe) < 5:
        ekraan.config(font = labelfont_56)
    if len(tehe) > 10:
        ekraan.config(font = labelfont_42)
    if len(tehe) > 13:
        ekraan.config(font = labelfont_28)
    sõne.set("".join(tehe))

def number7():
    if len(tehe) > 39:
        return
    tehe.append("7")
    if len(tehe) < 5:
        ekraan.config(font = labelfont_56)
    if len(tehe) > 10:
        ekraan.config(font = labelfont_42)
    if len(tehe) > 13:
        ekraan.config(font = labelfont_28)
    sõne.set("".join(tehe))

def number8():
    if len(tehe) > 39:
        return
    tehe.append("8")
    if len(tehe) < 5:
        ekraan.config(font = labelfont_56)
    if len(tehe) > 10:
        ekraan.config(font = labelfont_42)
    if len(tehe) > 13:
        ekraan.config(font = labelfont_28)
    sõne.set("".join(tehe))

def number9():
    if len(tehe) > 39:
        return
    tehe.append("9")
    if len(tehe) < 5:
        ekraan.config(font = labelfont_56)
    if len(tehe) > 10:
        ekraan.config(font = labelfont_42)
    if len(tehe) > 13:
        ekraan.config(font = labelfont_28)
    sõne.set("".join(tehe))

def punkt():
    if len(tehe) > 39:
        return
    tehe.append(".")
    if len(tehe) < 5:
        ekraan.config(font = labelfont_56)
    if len(tehe) > 10:
        ekraan.config(font = labelfont_42)
    if len(tehe) > 13:
        ekraan.config(font = labelfont_28)
    sõne.set("".join(tehe))


    
def võrdus():
    j = 0
    i = 0
    l = 0
    ruutjuure_alg = 0
    ruutjuure_lõpp = 0
    sulgemata = 0
    for element in tehe:
        if element == str("^"): # Ekraanil kuvatava sümboli teisendamine Pythonile arusaadavaks astendamiseks
            tehe[j] = str("**")
        if element == str("√"): # Ruutjuure sulgude märkimine õige koha peale, sulgude kontroll
            tehe[j] = str("sqrt(")
            element = tehe[j]
            ruutjuure_alg = j
            for märk in tehe[j+1:]:
                if märk == "(":
                    for sulg in tehe[j+2:]:
                        if sulg == ")":
                            sulu_positsioon = j + l + 3
                            tehe.insert(sulu_positsioon, str(")"))
                            ruutjuure_lõpp = sulu_positsioon
                            i = 0
                            l = 0
                            break
                        l += 1
                    break
                if märk.isnumeric() == False:
                    sulu_positsioon = j + i + 1
                    tehe.insert(sulu_positsioon, str(")"))
                    ruutjuure_lõpp = sulu_positsioon
                    i = 0
                    l = 0
                    break
                if j + i + 2 == len(tehe):
                    sulu_positsioon = j + i + 2
                    tehe.insert(j + i + 2, str(")"))
                    ruutjuure_lõpp = sulu_positsioon
                    i = 0
                    l = 0
                    break
                else:
                    i += 1

        if "(" in element:  # Sulgude õigsuse kontroll, "(" liigsuse kontroll on tsüklist väljas
            sulgemata += 1
        if ")" in element:
            sulgemata -= 1
        if sulgemata < 0:
            sõne.set("Error 1")
            return
        if element == str("sqrt("): # Ruutjuure õigsuse kontroll             
            if tehe[j+1] != str("("):
                if tehe[j+1].isnumeric() == False:
                    if tehe[j+1] == "-":
                        pass
                    else:
                        sõne.set("Error 1")
                        return
            else:
                pass

        j += 1
    if sulgemata > 0:
        sõne.set("Error 1")
        return
  
 
        
    arvutus = "".join(tehe)
    x = eval(arvutus)
    x = round(x,6)
    if len(str(x)) < 5:
        ekraan.config(font = labelfont_56)
    if len(str(x)) > 10:
        ekraan.config(font = labelfont_42)
    if len(str(x)) > 13:
        ekraan.config(font = labelfont_28)
    sõne.set(x)
    del tehe[:]
    x = repr(x)
    tehe.append(x)
    

def pluss():
    if len(tehe) > 39:
        return
    tehe.append("+")
    if len(tehe) < 5:
        ekraan.config(font = labelfont_56)
    if len(tehe) > 10:
        ekraan.config(font = labelfont_42)
    if len(tehe) > 13:
        ekraan.config(font = labelfont_28)
    sõne.set("".join(tehe))
    
def miinus():
    if len(tehe) > 39:
        return
    tehe.append("-")
    if len(tehe) < 5:
        ekraan.config(font = labelfont_56)
    if len(tehe) > 10:
        ekraan.config(font = labelfont_42)
    if len(tehe) > 13:
        ekraan.config(font = labelfont_28)
    sõne.set("".join(tehe))

def tühjenda():
    del tehe[:]
    sõne.set("")

def korda():
    if len(tehe) > 39:
        return
    tehe.append("*")
    if len(tehe) < 5:
        ekraan.config(font = labelfont_56)
    if len(tehe) > 10:
        ekraan.config(font = labelfont_42)
    if len(tehe) > 13:
        ekraan.config(font = labelfont_28)
    sõne.set("".join(tehe))

def jagatud():
    if len(tehe) > 39:
        return
    tehe.append("/")
    if len(tehe) < 5:
        ekraan.config(font = labelfont_56)
    if len(tehe) > 10:
        ekraan.config(font = labelfont_42)
    if len(tehe) > 13:
        ekraan.config(font = labelfont_28)
    sõne.set("".join(tehe))

def vasak_sulg():
    if len(tehe) > 39:
        return
    tehe.append("(")
    if len(tehe) < 5:
        ekraan.config(font = labelfont_56)
    if len(tehe) > 10:
        ekraan.config(font = labelfont_42)
    if len(tehe) > 13:
        ekraan.config(font = labelfont_28)
    sõne.set("".join(tehe))

def parem_sulg():
    if len(tehe) > 39:
        return
    tehe.append(")")
    if len(tehe) < 5:
        ekraan.config(font = labelfont_56)
    if len(tehe) > 10:
        ekraan.config(font = labelfont_42)
    if len(tehe) > 13:
        ekraan.config(font = labelfont_28)
    sõne.set("".join(tehe))

def astmel():
    if len(tehe) > 39:
        return
    tehe.append("^")
    if len(tehe) < 5:
        ekraan.config(font = labelfont_56)
    if len(tehe) > 10:
        ekraan.config(font = labelfont_42)
    if len(tehe) > 13:
        ekraan.config(font = labelfont_28)
    sõne.set("".join(tehe))

def ruutjuur():
    if len(tehe) > 39:
        return
    tehe.append("√")
    if len(tehe) < 5:
        ekraan.config(font = labelfont_56)
    if len(tehe) > 10:
        ekraan.config(font = labelfont_42)
    if len(tehe) > 13:
        ekraan.config(font = labelfont_28)
    sõne.set("".join(tehe))

def ruut():
    if len(tehe) > 39:
        return
    tehe.append("^")
    tehe.append("2")
    if len(tehe) < 5:
        ekraan.config(font = labelfont_56)
    if len(tehe) > 10:
        ekraan.config(font = labelfont_42)
    if len(tehe) > 13:
        ekraan.config(font = labelfont_28)
    sõne.set("".join(tehe))

def kuup():
    if len(tehe) > 39:
        return
    tehe.append("^")
    tehe.append("3")
    if len(tehe) < 5:
        ekraan.config(font = labelfont_56)
    if len(tehe) > 10:
        ekraan.config(font = labelfont_42)
    if len(tehe) > 13:
        ekraan.config(font = labelfont_28)
    sõne.set("".join(tehe))

def kustuta():
    tehe.pop()
    sõne.set("".join(tehe))
    








raam = Tk()
raam.title("Kalkulaator")
tahvel = Canvas(raam,
                width = 0,
                height = 0,
                bg = "white")              
tahvel.grid()

labelfont_56 = ("Terminal", 56)
labelfont_42 = ("Terminal", 42)
labelfont_28 = ("Terminal", 28)

tausta_pilt = PhotoImage(file = "Taust.gif")
taust = Label(raam,
              image = tausta_pilt,
              compound = CENTER)
taust.image = tausta_pilt
taust.place(x = 0, y = 0, relwidth=1, relheight=1)

tehe = []
sõne = StringVar()


ekraan_pilt = PhotoImage(file = "testekraan.gif")
ekraan = Label(raam,
               image = ekraan_pilt,
               textvariable = sõne,
               compound = CENTER,
               bg = "black",
               wraplength = 400)
ekraan.image = ekraan_pilt
ekraan.grid(row = 0,
            column = 1,
            columnspan = 5,
            padx = 5,
            pady = 5)

nr_0_pilt = PhotoImage(file = "Number_0.gif")
number_0 = Button(raam,
                  image = nr_0_pilt,
                  bg = "black",
                  command = number0,
                  textvariable = sõne
                  )
number_0.image = nr_0_pilt
number_0.grid(row = 4,
              column = 1,
              padx = 5,
              pady = 5)

nr_1_pilt = PhotoImage(file = "Number_1.gif")
number_1 = Button(raam,
                  image = nr_1_pilt,
                  bg = "black",
                  command = number1
                  )
number_1.image = nr_1_pilt
number_1.grid(row = 3,
              column = 1,
              padx = 5,
              pady = 5)

nr_2_pilt = PhotoImage(file = "Number_2.gif")
number_2 = Button(raam,
                  image = nr_2_pilt,
                  bg = "black",
                  command = number2
                  )
number_2.image = nr_2_pilt
number_2.grid(row = 3,
              column = 2,
              padx = 5,
              pady = 5)

nr_3_pilt = PhotoImage(file = "Number_3.gif")
number_3 = Button(raam,
                  image = nr_3_pilt,
                  bg = "black",
                  command = number3
                  )
number_3.image = nr_3_pilt
number_3.grid(row = 3,
              column = 3,
              padx = 5,
              pady = 5)

nr_4_pilt = PhotoImage(file = "Number_4.gif")
number_4 = Button(raam,
                  image = nr_4_pilt,
                  bg = "black",
                  command = number4
                  )
number_4.image = nr_4_pilt
number_4.grid(row = 2,
              column = 1,
              padx = 5,
              pady = 5)

nr_5_pilt = PhotoImage(file = "Number_5.gif")
number_5 = Button(raam,
                  image = nr_5_pilt,
                  bg = "black",
                  command = number5
                  )
number_5.image = nr_5_pilt
number_5.grid(row = 2,
              column = 2,
              padx = 5,
              pady = 5)

nr_6_pilt = PhotoImage(file = "Number_6.gif")
number_6 = Button(raam,
                  image = nr_6_pilt,
                  bg = "black",
                  command = number6
                  )
number_6.image = nr_6_pilt
number_6.grid(row = 2,
              column = 3,
              padx = 5,
              pady = 5)

nr_7_pilt = PhotoImage(file = "Number_7.gif")
number_7 = Button(raam,
                  image = nr_7_pilt,
                  bg = "black",
                  command = number7
                  )
number_7.image = nr_7_pilt
number_7.grid(row = 1,
              column = 1,
              padx = 5,
              pady = 5)

nr_8_pilt = PhotoImage(file = "Number_8.gif")
number_8 = Button(raam,
                  image = nr_8_pilt,
                  bg = "black",
                  command = number8
                  )
number_8.image = nr_8_pilt
number_8.grid(row = 1,
              column = 2,
              padx = 5,
              pady = 5)

nr_9_pilt = PhotoImage(file = "Number_9.gif")
number_9 = Button(raam,
                  image = nr_9_pilt,
                  bg = "black",
                  command = number9
                  )
number_9.image = nr_9_pilt
number_9.grid(row = 1,
              column = 3,
              padx = 5,
              pady = 5)

punkt_pilt = PhotoImage(file = "..gif")
punkt = Button(raam,
           image = punkt_pilt,
           bg = "black",
           command = punkt
           )
punkt.image = punkt_pilt
punkt.grid(row = 4,
           column = 2,
           padx = 5,
           pady = 5)


võrdus_pilt = PhotoImage(file = "võrdus.gif")
võrdus = Button(raam,
                image = võrdus_pilt,
                bg = "black",
                command = võrdus
                )
võrdus.image = võrdus_pilt
võrdus.grid(row = 4,
            column = 4,
            columnspan = 2,
            padx = 5,
            pady = 5) 

pluss_pilt = PhotoImage(file = "pluss.gif")
pluss = Button(raam,
               image = pluss_pilt,
               bg = "black",
               command = pluss
               )
pluss.image = pluss_pilt
pluss.grid(row = 3,
           column = 4,
           padx = 5,
           pady = 5)

miinus_pilt = PhotoImage(file = "miinus.gif")
miinus = Button(raam,
                image = miinus_pilt,
                bg = "black",
                command = miinus
                )
miinus.image = miinus_pilt
miinus.grid(row = 3,
            column = 5,
            padx = 5,
            pady = 5
            )

tühjenda_pilt = PhotoImage(file = "tühjenda.gif")
tühjenda = Button(raam,
                  image = tühjenda_pilt,
                  bg = "black",
                  command = tühjenda
                  )
tühjenda.image = tühjenda_pilt
tühjenda.grid(row = 4,
              column = 3,
              padx = 5,
              pady = 5
              )

korda_pilt = PhotoImage(file = "korda.gif")
korda = Button(raam,
               image = korda_pilt,
               bg = "black",
               command = korda
              )
korda.image = korda_pilt
korda.grid(row = 2,
           column = 4,
           padx = 5,
           pady = 5
           )

jagatud_pilt = PhotoImage(file = "jagatud.gif")
jagatud = Button(raam,
                 image = jagatud_pilt,
                 bg = "black",
                 command = jagatud
                )
jagatud.image = jagatud_pilt
jagatud.grid(row = 2,
             column = 5,
             padx = 5,
             pady = 5
             )

vasak_sulg_pilt = PhotoImage(file = "vasak_sulg.gif")
vasak_sulg = Button(raam,
                 image = vasak_sulg_pilt,
                 bg = "black",
                 command = vasak_sulg
                 )  
vasak_sulg.image = vasak_sulg_pilt
vasak_sulg.grid(row = 1,
                column = 4,
                padx = 5,
                pady = 5
                )

parem_sulg_pilt = PhotoImage(file = "parem_sulg.gif")
parem_sulg = Button(raam,
                    image = parem_sulg_pilt,
                    bg = "black",
                    command = parem_sulg
                    )
parem_sulg.image = parem_sulg_pilt
parem_sulg.grid(row = 1,
                column = 5,
                padx = 5,
                pady = 5
                )

astmel_pilt = PhotoImage(file = "astmel.gif")
astmel = Button(raam,
                image = astmel_pilt,
                bg = "black",
                command = astmel
                )
astmel.image = astmel_pilt
astmel.grid(row = 5,
            column = 1,
            padx = 5,
            pady = 5)

ruutjuur_pilt = PhotoImage(file = "ruutjuurpix.gif")
ruutjuur = Button(raam,
                  image = ruutjuur_pilt,
                  bg = "black",
                  command = ruutjuur)
ruutjuur.image = ruutjuur_pilt
ruutjuur.grid(row = 5,
              column = 2,
              padx = 5,
              pady = 5)

ruut_pilt = PhotoImage(file = "ruut.gif")
ruut = Button(raam,
              image = ruut_pilt,
              bg = "black",
              command = ruut)
ruut.image = ruut_pilt
ruut.grid(row = 5,
          column = 3,
          padx = 5,
          pady = 5)

kuup_pilt = PhotoImage(file = "kuup.gif")
kuup = Button(raam,
              image = kuup_pilt,
              bg = "black",
              command = kuup)
kuup.image = kuup_pilt
kuup.grid(row = 5,
          column = 4,
          padx = 5,
          pady = 5)

kustuta_pilt = PhotoImage(file = "kustuta.gif")
kustuta = Button(raam,
                 image = kustuta_pilt,
                 bg = "black",
                 command = kustuta)
kuup.image = kustuta_pilt
kustuta.grid(row = 5,
             column = 5,
             padx = 5,
             pady = 5)



raam.mainloop()
